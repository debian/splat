//
// Parameters for 1 arc-second high resolution mode of operation
// Generated for Debian packaging by bdale@gag.com on 2009-09-08
//

#define HD_MODE 1

// how big an area to allow processing in 1 arc-second mode
//	1 x 1 Degrees  ---------    52 Megabytes minimum RAM
//	2 x 2 Degrees  ---------   225 Megabytes minimum RAM
//	3 x 3 Degrees  ---------   468 Megabytes minimum RAM
//	4 x 4 Degrees  ---------   855 Megabytes minimum RAM
//	5 x 5 Degrees  ---------  1305 Megabytes minimum RAM
//	6 x 6 Degrees  ---------  1890 Megabytes minimum RAM
//	7 x 7 Degrees  ---------  2565 Megabytes minimum RAM
//	8 x 8 Degrees  ---------  3330 Megabytes minimum RAM
#define NUMDEGREES	4

#define MAXPAGES (NUMDEGREES*NUMDEGREES)

